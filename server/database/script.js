const { Prisma } = require("prisma-binding");
const Promise = require("bluebird");
const _ = require("lodash");

const db = new Prisma({
  typeDefs: "./src/schema/generated/prisma.graphql", // the auto-generated GraphQL schema of the Prisma API
  endpoint: process.env.PRISMA_SERVER || "http://127.0.0.1:4467" // the endpoint of the Prisma API
});

const namespaceModel = db.mutation
  .createModel({
    data: {
      name: "Namespace",
      url: "_namespaces",
      system: true,
      attributes: {
        create: [
          {
            name: "name",
            hasModel: false,
            isArray: false,
            type: "String"
          },
          {
            name: "isPublic",
            hasModel: false,
            isArray: false,
            type: "Boolean"
          },
          {
            name: "isDiscoverable",
            hasModel: false,
            isArray: false,
            type: "Boolean"
          }
        ]
      }
    }
  })
  .catch(err => {
    console.error(err);
  });

const riverSystemNS = namespaceModel
  .then(model => {
    return db.mutation.createInstance({
      data: {
        name: "River System",
        model: {
          connect: { id: model.id }
        },
        system: true
      }
    });
  })
  .catch(err => {
    console.error(err);
  });

const agentModel = riverSystemNS.then(ns => {
  return db.mutation
    .createModel({
      data: {
        name: "Agent",
        url: "_agents",
        system: true,
        namespace: { connect: { id: ns.id } },
        attributes: {
          create: [
            {
              name: "name",
              hasModel: false,
              isArray: false,
              type: "String"
            }
          ]
        }
      }
    })
    .catch(err => {
      console.error(err);
    });
});

const riverletAgent = Promise.join(agentModel, riverSystemNS)
  .spread((model, ns) => {
    return db.mutation.createInstance({
      data: {
        name: "Riverlet",
        model: {
          connect: { id: model.id }
        },
        system: true,
        namespaces: {
          connect: { id: ns.id }
        }
      }
    });
  })
  .catch(err => {
    console.error(err);
  });

const updateNSModel = Promise.join(
  agentModel,
  namespaceModel,
  riverSystemNS
).spread((agent, ns, nsI) => {
  return db.mutation.updateModel({
    where: { url: "_namespaces" },
    data: {
      namespace: {
        connect: { id: nsI.id }
      },
      attributes: {
        create: [
          {
            name: "administrators",
            hasModel: true,
            isArray: true,
            model: { connect: { id: agent.id } }
          },
          {
            name: "editors",
            hasModel: true,
            isArray: true,
            model: { connect: { id: agent.id } }
          },
          {
            name: "viewers",
            hasModel: true,
            isArray: true,
            model: { connect: { id: agent.id } }
          }
        ]
      }
    }
  });
});

const nsAttributes = Promise.join(updateNSModel).spread(ns => {
  return db.query.model(
    { where: { id: ns.id } },
    `{ id name attributes { id name } }`
  );
});

const updateRiverSystem = Promise.join(
  riverSystemNS,
  nsAttributes,
  riverletAgent
).spread((instance, model, riverlet) => {
  const getAttr = attrName => {
    return _.chain(model)
      .get("attributes")
      .find({ name: attrName })
      .get("id")
      .value();
  };

  return db.mutation.updateInstance({
    where: { id: instance.id },

    data: {
      namespaces: { connect: { id: instance.id } },
      observations: {
        create: [
          {
            attribute: { connect: { id: getAttr("name") } },
            value: "River System"
          },
          {
            attribute: { connect: { id: getAttr("isDiscoverable") } },
            value: "true"
          },
          {
            attribute: { connect: { id: getAttr("isPublic") } },
            value: "true"
          },
          {
            attribute: { connect: { id: getAttr("administrators") } },
            instance: { connect: { id: riverlet.id } }
          }
        ]
      }
    }
  });
});
