const {GraphQLServer} = require('graphql-yoga')
const {Prisma, forwardTo} = require('prisma-binding')
const resolvers = require('./resolvers')

const server = new GraphQLServer({
  typeDefs: './src/schema/schema.graphql',
  resolvers,
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: './src/schema/generated/prisma.graphql', // the auto-generated GraphQL schema of the Prisma API
      endpoint: process.env.PRISMA_SERVER || 'http://127.0.0.1:4467', // the endpoint of the Prisma API
      debug: false, // log all GraphQL queries & mutations sent to the Prisma API
    }),
  }),
})
server.start(() => console.log('Server is running on localhost:4000'))
