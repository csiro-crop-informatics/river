const {forwardTo, request} = require('prisma-binding')

const Mutation = {
  createModel: forwardTo('db'),
  createAttribute: forwardTo('db'),
  updateModel: forwardTo('db'),
  createInstance: forwardTo('db'),
  deleteInstance: (parent, {where}, context, info) => {
    return context.db.mutation.updateInstance(
      {
        where: where,
        data: {isActive: false},
      },
      info,
    )
  },
}

module.exports = {Mutation}
