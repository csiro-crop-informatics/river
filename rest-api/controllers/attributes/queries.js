const FRAGMENT = `
fragment attribute on Attribute {
  id
  name
  __typename
  hasModel
  isArray
  type
  model {
    id
    name
    url
    __typename
  }
  parentModels {
    id
    name
    url
    __typename
  }
}
`

const LIST_QUERY = `
query ($offset: Int, $limit: Int){
  attributes (skip: $offset, first: $limit) {
    ...attribute
  }
  meta: attributesConnection{
    aggregate {
      count
    }
  }
}
${FRAGMENT}
`

const SINGLE_QUERY = `
query($id: ID!) {
  attribute(where: {id: $id}) {
    ...attribute
  }
}
${FRAGMENT}
`

const SINGLE_UPDATE = `
mutation($id: ID!, $data: AttributeUpdateInput!) {
  attribute: updateAttribute(where: {id: $id}, data: $data) {
    ...attribute
  }
}
${FRAGMENT}
`

const SINGLE_CREATE = `
mutation($data: AttributeCreateInput!) {
  attribute: createAttribute(data: $data) {
    ...attribute
  }
}
${FRAGMENT}
`

const SINGLE_DELETE = `
mutation($id: ID!) {
  deleteAttribute(where: {id: $id}) {
    id
  }
}
`

module.exports = {
  LIST_QUERY,
  SINGLE_QUERY,
  SINGLE_UPDATE,
  SINGLE_CREATE,
  SINGLE_DELETE,
  FRAGMENT,
}
