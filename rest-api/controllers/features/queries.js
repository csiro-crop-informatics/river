const modelQueries = require('../models/queries')

const FRAGMENT = `
fragment instance on Instance {
  id
  name
  __typename
  isActive
  model {
    id
    name
    url
    __typename
  }
  observations(where: {isActive: true}) {
    id
    __typename
    isActive
    value
    instance {
      id
      name
      __typename
      model {
        id
        name
        url
        __typename
      }
    }
    attribute {
      id
    }
  }
}
`

const LIST_QUERY = `
query ($offset: Int, $limit: Int, $model: ID!, $filter: [InstanceWhereInput!]){
  instances (where: { AND: [{ model: { id: $model }, isActive: true }, { AND: $filter }] }, skip: $offset, first: $limit) {
    ...instance
  }
  meta: instancesConnection(where: {model: {id: $model }, isActive: true} ) {
    aggregate {
      count
    }
  }
  model(where: {id: $model}) {
    ...model
  }
}
${FRAGMENT}
${modelQueries.FRAGMENT}
`

const SINGLE_QUERY = `
query($id: ID!, $model: ID!) {
  instance(where: {id: $id}) {
    ...instance
  }
  model(where: {id: $model}) {
    ...model
  }
}
${FRAGMENT}
${modelQueries.FRAGMENT}
`

const MODEL_QUERY = `
query($url: String!) {
  model (where: {url: $url}) {
    ...model
  }
}
${modelQueries.FRAGMENT}
`

const SINGLE_CREATE = `
mutation($data: InstanceCreateInput!) {
  createInstance(data: $data) {
    ...instance
  }
}
${FRAGMENT}
`

module.exports = {
  LIST_QUERY,
  MODEL_QUERY,
  SINGLE_CREATE,
  SINGLE_QUERY,
  FRAGMENT,
}
