const prisma = require('../../db')
const update = require('immutability-helper')
const _ = require('lodash')
const {createDataField} = require('./helpers')
const Promise = require('bluebird')

const {LIST_QUERY, SINGLE_QUERY, SINGLE_CREATE} = require('./queries')

exports.findAll = (req, res, next) => {
  req.query.filter = req.query.filter ? JSON.parse(req.query.filter) : []

  prisma
    .request(LIST_QUERY, {...req.query, model: req.model.id})
    .then(result => {
      if (result.errors) {
        console.error(result.errors)
      }

      const {instances, meta, model} = result.data
      const {attributes} = model

      result.data.instances = _.map(instances, instance => {
        const data = createDataField(attributes, instance.observations)

        return _.chain(instance)
          .set('data', data)
          .omit('observations')
          .value()
      })

      res.json(result)
    })
    .catch(next)
}

exports.findOne = (req, res, next) => {
  prisma
    .request(SINGLE_QUERY, {...req.params, model: req.model.id})
    .then(result => {
      const {instance, meta, model} = result.data
      const {attributes} = model

      const data = createDataField(attributes, instance.observations)

      result.data.instance = _.chain(instance)
        .set('data', data)
        .omit('observations')
        .value()

      res.json(result)
    })
    .catch(next)
}
