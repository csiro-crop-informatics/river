const prisma = require('../../db')
const _ = require('lodash')

const SINGLE_CREATE = `
mutation($name: String, $model: ID!, $observations: [ObservationCreateWithoutParentInput!]!) {
  instance: createInstance(data: { 
    name: $name,
    model: {
      connect: { id: $model }
    }
    observations: {
      create: $observations
    }
  }) {
    id
  }
}
`

function makeObservation(attribute, observation) {
  // If the attribute does not have a model just attch the value and attribute
  if (!attribute.hasModel) {
    return {value: observation, attribute: {connect: {id: attribute.id}}}
    // If the observation does have a model we can either create a new instance
    // of that model or connect an existing model
    //
    // If a "create" object is supplied we will create a new object else
    // we will just connect an existing argument
  } else if (!observation.create) {
    return {
      instance: {connect: {id: observation.id}},
      attribute: {connect: {id: attribute.id}},
    }
    // These operations are recursively nestable up until the point where we
    // do not have any more attributes available.
  } else if (observation.create) {
    return {
      instance: {
        create: {
          model: {connect: {id: attribute.model.id}},
          name: observation.create.name,
          observations: {
            create: makeObservationObjects(
              attribute.model.attributes,
              observation.create,
            ),
          },
        },
      },
      attribute: {connect: {id: attribute.id}},
    }
  }
}

function makeObservationObjects(possibleAttributes, dataObject) {
  return _.reduce(
    dataObject,
    (acc, obs, obsName) => {
      const attr = _.find(possibleAttributes, {name: obsName})

      if (!attr) {
        return acc
      }

      const makeVal = _.curry(makeObservation)(attr)

      if (attr.isArray) {
        return _.concat(acc, _.map(obs, makeVal))
      } else {
        return _.concat(acc, [makeVal(obs)])
      }
    },
    [],
  )
}

exports.createOne = (req, res, next) => {
  const attributes = _.get(req, 'model.attributes')
  const reqObservations = _.get(req, 'body.data', {})

  const observations = makeObservationObjects(attributes, reqObservations)

  prisma
    .request(SINGLE_CREATE, {
      name: req.body.name,
      model: req.model.id,
      observations: observations,
    })
    .then(result => {
      if (result.errors) {
        return res.status(400).json(result.errors)
      }

      if (!result.data.instance.id) {
        console.log(result)
        return res.status(400).json({message: 'Resource not created'})
      }

      console.log(
        'Redirecting to',
        `/${req.model.url}/${result.data.instance.id}`,
      )
      return res.redirect(303, `/${req.model.url}/${result.data.instance.id}`)
    })
    .catch(err => {
      next()
    })
}
