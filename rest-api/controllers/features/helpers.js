const _ = require('lodash')

exports.createDataField = function createDataField(attributes, observations) {
  const data = _.reduce(
    attributes,
    (acc, attribute) => {
      let result = null

      if (!attribute.isArray && !attribute.hasModel) {
        result = _.chain(observations)
          .find(observation => {
            return observation.attribute.id === attribute.id
          })
          .get('value')
          .value()
      } else if (attribute.isArray && !attribute.hasModel) {
        result = _.chain(observations)
          .filter(observation => {
            return observation.attribute.id === attribute.id
          })
          .map('value')
          .value()
      } else if (!attribute.isArray && attribute.hasModel) {
        result = _.chain(observations)
          .find(observation => {
            return observation.attribute.id === attribute.id
          })
          .get('instance')
          .value()
      } else if (attribute.isArray && attribute.hasModel) {
        result = _.chain(observations)
          .filter(observation => {
            return observation.attribute.id === attribute.id
          })
          .map(ob => _.get(ob, 'instance'))
          .value()
      }

      return {
        ...acc,
        [attribute.name]: result,
      }
    },
    {},
  )

  return data
}
