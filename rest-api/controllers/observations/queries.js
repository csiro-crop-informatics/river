const FRAGMENT = `
fragment observation on Observation {
  id
  value
  __typename
  createdAt
  updatedAt
  deletedAt
  isActive
  attribute {
    id
    name
    __typename
    hasModel
    isArray
    type
    model {
      id
      __typename
      name
      url
    }
  }
  instance {
    id
    name
    __typename
    model {
      id
      name
      url
      __typename
    }
  }
  parent {
    id
    name
    __typename
    model {
      id
      name
      url
      __typename
    }
  }
}
`

const LIST_QUERY = `
query ($offset: Int, $limit: Int){
  observations (skip: $offset, first: $limit) {
    ...observation
  }
  meta: observationsConnection{
    aggregate {
      count
    }
  }
}
${FRAGMENT}
`

const SINGLE_QUERY = `
query($id: ID!) {
  observation(where: {id: $id}) {
    ...observation
  }
}
${FRAGMENT}
`

const SINGLE_UPDATE = `
mutation($id: ID!, $data: ObservationUpdateInput!) {
  observation: updateObservation(where: {id: $id}, data: $data) {
    ...observation
  }
}
${FRAGMENT}
`

const SINGLE_CREATE = `
mutation($data: ObservationCreateInput!) {
  observation: createObservation(data: $data) {
    ...observation
  }
}
${FRAGMENT}
`

const SINGLE_DELETE = `
mutation($id: ID!) {
  deleteObservation(where: {id: $id}) {
    id
  }
}
`

module.exports = {
  LIST_QUERY,
  SINGLE_QUERY,
  SINGLE_UPDATE,
  SINGLE_CREATE,
  SINGLE_DELETE,
  FRAGMENT,
}
