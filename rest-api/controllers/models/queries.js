const FRAGMENT = `
fragment model on Model {
  id
  name
  url
  __typename
  attributes {
    id
    name
    hasModel
    isArray
    __typename
    model {
      id
      __typename
      name
      url
      attributes {
        id
        name
        __typename
        hasModel
        isArray
        model {
          id
          __typename
          name
          url
        }
      }
    }
  }
  parentAttributes {
    id
    name
    __typename
    parentModels {
      id
      name
      url
      __typename
    }
  }
}
`

const LIST_QUERY = `
query ($offset: Int, $limit: Int){
  models (skip: $offset, first: $limit) {
    ...model
  }
  meta: modelsConnection{
    aggregate {
      count
    }
  }
}
${FRAGMENT}
`

const SINGLE_QUERY = `
query($id: ID!) {
  model(where: {id: $id}) {
    ...model
  }
}
${FRAGMENT}
`

const SINGLE_UPDATE = `
mutation($id: ID!, $data: ModelUpdateInput!) {
  updateModel(where: {id: $id}, data: $data) {
    ...model
  }
}
${FRAGMENT}
`

const SINGLE_CREATE = `
mutation($data: ModelCreateInput!) {
  createModel(data: $data) {
    ...model
  }
}
${FRAGMENT}
`

const SINGLE_DELETE = `
mutation($id: ID!) {
  deleteModel(where: {id: $id}) {
    id
  }
}
`

module.exports = {
  LIST_QUERY,
  SINGLE_QUERY,
  SINGLE_UPDATE,
  SINGLE_CREATE,
  SINGLE_DELETE,
  FRAGMENT,
}
