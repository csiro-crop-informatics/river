const express = require('express')
const router = express.Router()
const controller = require('../controllers/features')

router.get('/:instance/', controller.findAll)
router.get('/:instance/:id', controller.findOne)
router.put('/:instance/:id', controller.updateOne)
router.post('/:instance/', controller.createOne)
router.delete('/:instance/:id', controller.deleteOne)

router.param('instance', controller.getModel)

module.exports = router
