const express = require('express')
const app = express()
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./docs/swagger.js')

const options = {
  explorer: false,
  customSiteTitle: '🏞️',
  customfavIcon: '🏞️',
  customCss: `
.swagger-ui .topbar {
  background: linear-gradient(45deg, #61affe, 25%, #b961fe);
}

.swagger-ui .opblock-summary-get {
  background: linear-gradient(45deg, rgba(97, 175, 254, .3), 40%, rgba(185, 97, 254, .3));
}

.swagger-ui .opblock-summary-post {
  background: linear-gradient(45deg, rgba(73, 204, 144, .3), 40%, rgba(185, 97, 254, .3));
}
  
.swagger-ui .opblock-summary-put {
  background: linear-gradient(45deg, rgba(252, 161, 48, .3), 40%, rgba(185, 97, 254, .3));
}

.swagger-ui .opblock-summary-delete {
  background: linear-gradient(45deg, rgba(249, 62, 62, .3), 40%, rgba(185, 97, 254, .3));
}

.swagger-ui .topbar .wrapper img {
  display: none;
}

.swagger-ui .topbar .wrapper span {
  width: 100%;
  color: rgba(0, 0, 0, 0);
}

.swagger-ui .topbar .wrapper span:before {
  content: '🏞️ River';
  word-wrap: break-word;
  color: white;
}
`,
}
let docs = swaggerDocument()
setInterval(() => {
  docs = swaggerDocument()
  app.use(swaggerUi.serve, swaggerUi.setup(docs, options))
}, 5000)
app.use(swaggerUi.serve, swaggerUi.setup(docs, options))
app.listen(4060)
