const attributePathDocs = require('./standard-docs/attribute-path')
const attributes = require('./standard-docs/attribute-component')
const observationPathDocs = require('./standard-docs/observation-path')
const observations = require('./standard-docs/observation-component')
const modelPathDocs = require('./standard-docs/model-path')

const models = require('./standard-docs/model-components')
const connectionComponent = require('./standard-docs/connection-components')

const mkInstanceDocs = require('./generated-docs/make-instance-docs')
const mkModels = require('./generated-docs/make-model-docs')
const config = require('./config')
const _ = require('lodash')

module.exports = () => ({
  openapi: '3.0.2',
  info: {
    title: config.name,
    description: 'The Rest version of the River API',
    version: config.version,
  },
  servers: [
    {
      url: 'https://api.tomorrowstodayslater.xyz',
      description: 'Demo API Server',
    },
    {url: 'http://127.0.0.1:3005', description: 'Default Local Service'},
  ],
  paths: _.merge(
    {},
    modelPathDocs,
    attributePathDocs,
    observationPathDocs,
    mkInstanceDocs(),
  ),
  components: {
    schemas: _.merge(
      {},
      mkModels(),
      models,
      attributes,
      observations,
      connectionComponent,
    ),
  },
})
