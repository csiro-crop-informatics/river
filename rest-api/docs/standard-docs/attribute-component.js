module.exports = {
  _attribute: {
    title: 'Attribute',
    type: 'object',
    properties: {
      id: {
        type: 'string',
        format: 'cuid',
        example: 'cjo3j0ngc00470924auczhlu2',
      },
      name: {
        type: 'string',
        example: 'Height',
        description: 'Name of attribute',
      },
      uri: {
        type: 'string',
        example: 'http://localhost:3005/_attributes/cjo3i8o9v002j0924hav8d1pq',
        description: 'A URI to get more information',
      },
      hasModel: {
        type: 'boolean',
        example: 'true',
        description: 'Is attribute associated with a model',
      },
      isArray: {
        type: 'boolean',
        example: 'false',
        description: 'Does attribute retain an array of observations',
      },
      type: {
        type: 'string',
        example: 'Attribute value type',
        enum: ['string', 'integer', 'float', 'boolean'],
        default: 'string',
      },
      model: {
        type: 'object',
        description:
          'The model type this observation can record a connection to',
        properties: {
          id: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
          name: {
            type: 'string',
            example: 'Experiment',
          },
          url: {
            type: 'string',
            description: 'The Url part to access the model',
            example: 'experiment',
          },
          uri: {
            type: 'string',
            example: 'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
            description: 'A URI to get more information',
          },
        },
      },
      parentModels: {
        type: 'array',
        description: 'A list of models that implement this attribute',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'string',
              format: 'cuid',
              example: 'cjo3i8o9v002j0924hav8d1pq',
            },
            name: {
              type: 'string',
              example: 'Experiment',
            },
            url: {
              type: 'string',
              description: 'The Url part to access the model',
              example: 'experiment',
            },
            uri: {
              type: 'string',
              example:
                'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
              description: 'A URI to get more information',
            },
          },
        },
      },
    },
  },
}
