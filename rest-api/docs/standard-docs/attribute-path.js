const config = require('../config')

module.exports = {
  '/_attributes': {
    get: {
      tags: ['Attributes'],
      summary: 'A list of attributes',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Attributes',
      parameters: [
        {
          name: 'limit',
          in: 'query',
          required: false,
          description: 'The maximum number of attributes to be returned.',
          schema: {
            type: 'integer',
            default: 1000,
          },
        },
        {
          name: 'offset',
          in: 'query',
          required: false,
          description: 'The number of attributes to offset the result by.',
          schema: {
            type: 'integer',
            default: 0,
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'Data',
                    type: 'object',
                    properties: {
                      attributes: {
                        title: 'Attributes',
                        type: 'array',
                        items: {
                          $ref: '#/components/schemas/_attribute',
                        },
                      },
                      attributeConnection: {
                        $ref: '#/components/schemas/_connection',
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    post: {
      tags: ['Attributes'],
      summary: 'Create an attribute',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Attribute Body',
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'Height',
                  description: 'Name of attribute',
                },
                hasModel: {
                  type: 'boolean',
                  example: 'true',
                  description: 'Is attribute associated with a model',
                },
                isArray: {
                  type: 'boolean',
                  example: 'false',
                  description: 'Does attribute retain an array of observations',
                },
                type: {
                  type: 'string',
                  example: 'Attribute value type',
                  enum: ['string', 'integer', 'float', 'boolean'],
                  default: 'string',
                },
                model: {
                  $ref: '#/components/schemas/_connect',
                },
              },
            },
          },
        },
      },
      operationId: 'POST: Attribute',
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  attributes: {
                    title: 'Attributes',
                    type: 'array',
                    items: {
                      $ref: '#/components/schemas/_attribute',
                    },
                  },
                  attributesConnection: {
                    $ref: '#/components/schemas/_connection',
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  '/_attributes/{id}': {
    get: {
      tags: ['Attributes'],
      summary: 'A single of attribute',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Attribute',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the attribute to return',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  attribute: {
                    $ref: '#/components/schemas/_attribute',
                  },
                },
              },
            },
          },
        },
      },
    },
    put: {
      tags: ['Attributes'],
      summary: 'Update of an attribute name',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'PUT: Attribute',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the attribute to update',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Attribute Body',
              type: 'object',
              properties: {
                name: {
                  type: 'string',
                  example: 'Height',
                  description: 'Name of attribute',
                },
              },
            },
          },
        },
      },
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  attribute: {
                    $ref: '#/components/schemas/_attribute',
                  },
                },
              },
            },
          },
        },
      },
    },
    delete: {
      tags: ['Attributes'],
      summary: 'Delete a single attribute',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'DELETE: Attribute',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the attribute to delete',
          schema: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
        {
          name: 'confirm',
          in: 'query',
          required: true,
          description:
            'Ensures that the user is aware of the implications of deleting an attribute',
          schema: {
            type: 'boolean',
            example: 'true',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'data',
                    type: 'object',
                    properties: {
                      deleteAttribute: {
                        type: 'object',
                        properties: {
                          id: {
                            type: 'string',
                            format: 'cuid',
                            example: 'cjo55zor6000n0b246pjmiehb',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}
