module.exports = {
  _model: {
    title: 'Model',
    type: 'object',
    properties: {
      id: {
        type: 'string',
        format: 'cuid',
        example: 'cjo3j0ngc00470924auczhlu2',
      },
      name: {
        type: 'string',
        example: 'My Model',
      },
      uri: {
        type: 'string',
        example: 'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
        description: 'A URI to get more information',
      },
      parentAttributes: {
        type: 'array',
        description: 'A list of attributes that refer to this model',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'string',
              format: 'cuid',
              example: 'cjo3j0ngc00470924auczhlu2',
            },
            name: {
              type: 'string',
              example: 'My Attribute',
            },
            uri: {
              type: 'string',
              example:
                'http://localhost:3005/_attributes/cjo3i8o9v002j0924hav8d1pq',
              description: 'A URI to get more information',
            },
            model: {
              type: 'object',
              description: 'The model where this attribute comes from',
              properties: {
                id: {
                  type: 'string',
                  format: 'cuid',
                  example: 'cjo3j0ngc00470924auczhlu2',
                },
                name: {
                  type: 'string',
                  example: 'My Other Model',
                },
                uri: {
                  type: 'string',
                  example:
                    'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
                  description: 'A URI to get more information',
                },
              },
            },
          },
        },
      },
      attributes: {
        type: 'array',
        description:
          'A list of attributes that instances of this model may have',
        items: {
          type: 'object',
          properties: {
            id: {
              type: 'string',
              format: 'cuid',
              example: 'cjo3j0ngc00470924auczhlu2',
            },
            name: {
              type: 'string',
              example: 'My Attribute',
            },
            url: {
              type: 'string',
              example: 'The url part to access the model',
            },
            uri: {
              type: 'string',
              example:
                'http://localhost:3005/_attributes/cjo3i8o9v002j0924hav8d1pq',
              description: 'A URI to get more information',
            },
            model: {
              type: 'object',
              description: 'The model where this attribute comes from',
              properties: {
                id: {
                  type: 'string',
                  format: 'cuid',
                  example: 'cjo3j0ngc00470924auczhlu2',
                },
                name: {
                  type: 'string',
                  example: 'My Other Model',
                },
                url: {
                  type: 'string',
                  example: 'The Url part to access the model',
                },
                uri: {
                  type: 'string',
                  example:
                    'http://localhost:3005/_models/cjo3i8o9v002j0924hav8d1pq',
                  description: 'A URI to get more information',
                },
              },
            },
          },
        },
      },
    },
  },
}
