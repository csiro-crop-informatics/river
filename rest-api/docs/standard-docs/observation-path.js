const config = require('../config')

module.exports = {
  '/_observations': {
    get: {
      tags: ['Observations'],
      summary: 'A list of observations',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Observations',
      parameters: [
        {
          name: 'limit',
          in: 'query',
          required: false,
          description: 'The maximum number of observations to be returned.',
          schema: {
            type: 'integer',
            default: 1000,
          },
        },
        {
          name: 'offset',
          in: 'query',
          required: false,
          description: 'The number of observations to offset the result by.',
          schema: {
            type: 'integer',
            default: 0,
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'Data',
                    type: 'object',
                    properties: {
                      observations: {
                        title: 'Observations',
                        type: 'array',
                        items: {
                          $ref: '#/components/schemas/_observation',
                        },
                      },
                      observationConnection: {
                        $ref: '#/components/schemas/_connection',
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
    post: {
      tags: ['Observations'],
      summary: 'Create an observation',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Observation Body',
              type: 'object',
              properties: {
                value: {
                  type: 'string',
                  example: '5.0',
                  description: 'Observation value',
                },
                attribute: {
                  $ref: '#/components/schemas/_connect',
                },
                instance: {
                  $ref: '#/components/schemas/_connect',
                },
                childInstance: {
                  $ref: '#/components/schemas/_connect',
                },
              },
            },
          },
        },
      },
      operationId: 'POST: Observation',
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  observations: {
                    title: 'Observations',
                    type: 'array',
                    items: {
                      $ref: '#/components/schemas/_observation',
                    },
                  },
                  observationsConnection: {
                    $ref: '#/components/schemas/_connection',
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  '/_observations/{id}': {
    get: {
      tags: ['Observations'],
      summary: 'A single of observation',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'GET: Observation',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the observation to return',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  observation: {
                    $ref: '#/components/schemas/_observation',
                  },
                },
              },
            },
          },
        },
      },
    },
    put: {
      tags: ['Observations'],
      summary: 'Update of an observation value',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'PUT: Observation',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the observation to update',
          schema: {
            type: 'string',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
      ],
      requestBody: {
        description: 'The request body',
        required: true,
        content: {
          'application/json': {
            schema: {
              title: 'Observation Body',
              type: 'object',
              properties: {
                value: {
                  type: 'string',
                  example: '7.5',
                  description: 'Updated observation value',
                },
              },
            },
          },
        },
      },
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  observation: {
                    $ref: '#/components/schemas/_observation',
                  },
                },
              },
            },
          },
        },
      },
    },
    delete: {
      tags: ['Observations'],
      summary: 'Delete a single observation',
      externalDocs: {
        url: config.externalDocs,
        description: 'River Documentation Page',
      },
      operationId: 'DELETE: Observation',
      parameters: [
        {
          name: 'id',
          in: 'path',
          required: true,
          description: 'The Id of the observation to delete',
          schema: {
            type: 'string',
            format: 'cuid',
            example: 'cjo3i8o9v002j0924hav8d1pq',
          },
        },
        {
          name: 'confirm',
          in: 'query',
          required: true,
          description:
            'Ensures that the user is aware of the implications of deleting an observation',
          schema: {
            type: 'boolean',
            example: 'true',
          },
        },
      ],
      responses: {
        200: {
          description: 'Successful response',
          content: {
            'application/json': {
              schema: {
                title: 'Data Object',
                type: 'object',
                properties: {
                  data: {
                    title: 'data',
                    type: 'object',
                    properties: {
                      deleteObservation: {
                        type: 'object',
                        properties: {
                          id: {
                            type: 'string',
                            format: 'cuid',
                            example: 'cjo55zor6000n0b246pjmiehb',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      },
    },
  },
}
