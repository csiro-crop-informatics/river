module.exports = {
  _connect: {
    title: 'Create a new relationship',
    type: 'object',
    properties: {
      connect: {
        type: 'object',
        properties: {
          id: {
            title: 'The id of the foreign entity',
            type: 'string',
            format: 'cuid',
            example: 'cjo3iz2nx003w0924plb82c6r',
          },
        },
      },
    },
  },
  _connectOrDisconnect: {
    title: 'Create or remove new relationship',
    type: 'object',
    properties: {
      disconnect: {
        type: 'object',
        properties: {
          id: {
            title: 'The id of the foreign entity to disconnect',
            type: 'string',
            format: 'cuid',
            example: 'cjo3iz2nx003w0924plb82c6r',
          },
        },
      },
      connect: {
        type: 'object',
        properties: {
          id: {
            title: 'The id of the foreign entity',
            type: 'string',
            format: 'cuid',
            example: 'cjo3iz2nx003w0924plb82c6r',
          },
        },
      },
    },
  },
  _connection: {
    title: 'Connection Information',
    type: 'object',
    properties: {
      aggregate: {
        title: 'Aggregate',
        type: 'object',
        properties: {
          count: {
            title: 'Count',
            type: 'integer',
            description: 'The total number of models regardles of limits',
          },
        },
      },
    },
  },
}
