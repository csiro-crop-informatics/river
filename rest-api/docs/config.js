module.exports = {
  name: '🏞️',
  externalDocs: 'https://docs.tomorrowstodayslater.xyz',
  version: '0.0.1',
  serverUrl: 'http://localhost:3005',
}
