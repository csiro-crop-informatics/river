const Promise = require('bluebird')
const prisma = require('../../db')
const ejs = require('ejs')
const tFile = require('fs').readFileSync(
  __dirname + '/model-template.json',
  'utf8',
)
const template = ejs.compile(tFile)
const _ = require('lodash')
const config = require('../config')

let paths = {}

function mkDocs() {
  const QUERY = `
    query {
      models {
        id
        name
        url
        attributes {
          id
          name
          hasModel
          isArray
          type
          model {
            id
            name
            url
          }
        }
      }
    }
  `
  prisma
    .request(QUERY)
    .then(result => {
      let newPaths = {}

      _.forEach(result.data.models, model => {
        const attrs = model.attributes.map(makeAttributeSchema).join(', ')

        const temp = template({config: config, ...model, attrs})

        newPaths = _.merge(newPaths, JSON.parse(temp))
      })

      paths = newPaths
    })
    .catch(err => console.error(err))
}

setInterval(mkDocs, 5000)
mkDocs()

function makeAttributeSchema(attr) {
  const type = attr.type ? attr.type.toLowerCase() : 'string'
  if (!attr.hasModel && !attr.isArray) {
    return `
      "${attr.name}": {
        "type": "${type}"
      }
    `
  } else if (!attr.hasModel && attr.isArray) {
    return `
      "${attr.name}": {
        "type": "array",
        "items": {
          "type": "${type}"
        }
      }
    `
  } else if (attr.hasModel && !attr.isArray) {
    return `
      "${attr.name}": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "format": "cuid",
            "example": "cjo3j0ngc00470924auczhlu2"
          },
          "name": {
            "type": "string",
            "example": "My ${attr.model.name}"
          },
          "uri": {
            "type": "string",
            "example": "${config.serverUrl}/${
      attr.model.url
    }/cjo3j0ngc00470924auczhlu2"
          }
        }
      }
    `
  } else {
    return `
      "${attr.name}": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "id": {
              "type": "string",
              "format": "cuid",
              "example": "cjo3j0ngc00470924auczhlu2"
            },
          "name": {
            "type": "string",
            "example": "My ${attr.model.name}"
          },
          "uri": {
            "type": "string",
            "example": "${config.serverUrl}/${
      attr.model.url
    }/cjo3j0ngc00470924auczhlu2"
          }
          }
        }
      }
    `
  }
}

module.exports = () => paths
