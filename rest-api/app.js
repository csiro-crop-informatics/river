var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const _ = require('lodash')
const cors = require('cors')()

var modelRouter = require('./routes/models')
var featureRouter = require('./routes/features')
var attributeRouter = require('./routes/attributes')
var observationRouter = require('./routes/observations')
var idRouter = require('./routes/id')

var app = express()

app.set(
  'json replacer',

  function replacer(key, value) {
    const mapper = () => {
      switch (value.__typename) {
        case 'Model':
          return '_models'
        case 'Attribute':
          return '_attributes'
        case 'Observation':
          return '_observations'
        case 'Namespace':
          return '_namespaces'
        case 'Instance':
          return _.get(value, 'model.url', '_id')
      }
    }

    const server = process.env.SERVER || 'http://localhost:3005'

    if (_.get(value, '__typename') && _.get(value, 'id')) {
      value.uri = `${server}/${mapper()}/${value.id}`
      delete value.__typename
    }
    return value
  },
)

app.set('x-powered-by', false)

app.use(cors)
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use((req, res, next) => {
  req.query.limit = parseInt(req.query.limit, 10)
  req.query.offset = parseInt(req.query.offset, 10)

    req.query.limit = req.query.limit || undefined
    req.query.offset = req.query.offset || undefined

  next()
})

app.use('/_models', modelRouter)
app.use('/_observations', observationRouter)
app.use('/_attributes', attributeRouter)
app.use('/_id', idRouter)
app.use(featureRouter)

app.use((err, req, res, next) => {
    res.status(500).json(err.message)
})

module.exports = app
