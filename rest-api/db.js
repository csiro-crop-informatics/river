const {Prisma} = require('prisma-binding')

const prisma = new Prisma({
  endpoint: process.env.PRISMA_SERVER || 'http://127.0.0.1:4467',
  typeDefs: './schema/generated/prisma.graphql',
  debug: false,
})

module.exports = prisma
