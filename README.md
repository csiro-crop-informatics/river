# River 🏞️ 

[![pipeline status](https://gitlab.com/csiro-crop-informatics/river/badges/master/pipeline.svg)](https://gitlab.com/csiro-crop-informatics/river/commits/master)

[Docs](https://docs.tomorrowstodayslater.xyz)

[Demo](https://app.tomorrowstodayslater.xyz)