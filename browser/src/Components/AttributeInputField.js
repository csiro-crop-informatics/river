import React, {Component} from 'react'

import TextField from '@material-ui/core/TextField'
import ConnectOrCreate from './ConnectOrCreate'

import gql from 'graphql-tag'

class AttributeInputField extends Component {
  onChange = attribute => e => {
    this.props.onChange({
      attribute: {connect: {id: attribute.id}},
      value: e.target.value,
    })
  }

  render() {
    const {attribute} = this.props

    if (attribute.hasModel) {
      const SEARCH = gql`
      query instances($input: String!) {
        instances(
          where: {AND: [{name_contains: $input}, {model: {id: "${
            attribute.model.id
          }"}}, {isActive: true}]}
          first: 5
        ) {
          id
          name
        }
      }
    `

      const ID_SEARCH = gql`
      query instances($input: ID!) {
        instances(
          where: {AND: [{id_contains: $input}, {model: {id: "${
            attribute.model.id
          }"}}, {isActive: true}]}
          first: 5
        ) {
          id
          name
        }
      }
    `

      return (
        <ConnectOrCreate
          searchTerms={SEARCH}
          idSearchTerms={ID_SEARCH}
          onChange={this.props.onChange}
          attribute={attribute}
        />
      )
    }

    return (
      <TextField
        onChange={this.onChange(attribute)}
        label={attribute.name}
        fullWidth
        variant="outlined"
        margin="normal"
      />
    )
  }
}

export default AttributeInputField
