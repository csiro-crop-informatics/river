import React from "react"
import PropTypes from "prop-types"
import classNames from "classnames"
import { withStyles } from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "./NavList"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import { Link } from "react-router-dom"

import styles from "./styles"

class MiniDrawer extends React.Component {
  render() {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <AppBar position="absolute" className={classNames(classes.appBar)}>
          <Toolbar disableGutters={false}>
            <Typography variant="title" color="inherit" noWrap>
              <Link className={classes.link} to="/">
                <span role="img" aria-labelledby="river">
                  🏞️
                </span>{" "}
                River
              </Link>
            </Typography>
            <div className={classes.grow} />
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(classes.drawerPaper)
          }}
          open={true}
        >
          <div className={classes.drawerHeader} />
          <Divider />
          <List />
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {this.props.children}
        </main>
      </div>
    )
  }
}
MiniDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
}
export default withStyles(styles, { withTheme: true })(MiniDrawer)
