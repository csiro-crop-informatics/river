import React from 'react'

import {Link} from 'react-router-dom'

import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'

import {withStyles} from '@material-ui/core/styles'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {map, get} from 'lodash'

const QUERY = gql`
  query {
    models {
      id
      url
      name
    }
  }
`

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4,
  },
})

class NavList extends React.Component {
  render() {
    console.log(this.props.data)
    const models = get(this, 'props.data.models', [])
    console.log(models[0])
    return (
      <List>
        <Link to="/models" style={{textDecoration: 'none', color: 'inherit'}}>
          <ListItem button>
            <ListItemText primary="Vocabulary Builder" />
          </ListItem>
        </Link>
        <Divider />
        <Link to="/resolver" style={{textDecoration: 'none', color: 'inherit'}}>
          <ListItem button>
            <ListItemText primary="ID Resolver" />
          </ListItem>
        </Link>
        <Link
          to="/graph-viz"
          style={{textDecoration: 'none', color: 'inherit'}}>
          <ListItem button>
            <ListItemText primary="Model Visualization" />
          </ListItem>
        </Link>
        <Divider />
        {map(models, model => {
          return (
            <Link
              key={model.id}
              to={`/instance/${model.url}`}
              style={{textDecoration: 'none', color: 'inherit'}}>
              <ListItem button>
                <ListItemText primary={model.name} />
              </ListItem>
            </Link>
          )
        })}
      </List>
    )
  }
}

export default graphql(QUERY, {
  options: {pollInterval: 5000},
})(withStyles(styles)(NavList))
