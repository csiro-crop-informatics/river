import React, {Component} from 'react'

import {Query} from 'react-apollo'
import Downshift from 'downshift'
import {withRouter} from 'react-router-dom'
import Error from './UI/Errors'

import Paper from '@material-ui/core/Paper'
import ListItem from '@material-ui/core/ListItem'
import Avatar from '@material-ui/core/Avatar'
import ListItemText from '@material-ui/core/ListItemText'
import CircularProgress from '@material-ui/core/CircularProgress'
import TextField from '@material-ui/core/TextField'
import {get, map} from 'lodash'

class SearchType extends Component {
  render() {
    return (
      <Downshift
        itemToString={item => (item ? item.name : '')}
        onChange={this.props.onChange}>
        {({
          inputValue,
          getInputProps,
          getMenuProps,
          getItemProps,
          selectedItem,
          highlightedIndex,
          isOpen,
        }) => {
          return (
            <div>
              <TextField
                label={`Search ${this.props.label}`}
                variant="outlined"
                fullWidth
                margin="normal"
                {...getInputProps()}
              />
              <AutoCompleteMenu
                {...{
                  inputValue,
                  getMenuProps,
                  getItemProps,
                  selectedItem,
                  highlightedIndex,
                  isOpen,
                  history: this.props.history,
                  query: this.props.query,
                }}
              />
            </div>
          )
        }}
      </Downshift>
    )
  }
}

function AutoCompleteMenu({
  selectedItem,
  highlightedIndex,
  isOpen,
  getItemProps,
  getMenuProps,
  inputValue,
  history,
  query,
}) {
  if (!isOpen) {
    return null
  }

  return (
    <Query query={query} variables={{input: inputValue}}>
      {({loading, error, data}) => {
        const entityName = get(query, 'definitions[0].name.value')

        const allTypes = (data && data[entityName]) || []

        if (loading) {
          return (
            <Paper>
              <ListItem>
                <Avatar>
                  <CircularProgress size={25} color="secondary" />
                </Avatar>
                <ListItemText primary="Loading" />
              </ListItem>
            </Paper>
          )
        }

        if (error) {
          return <Error error={error} />
        }

        return (
          <Paper {...getMenuProps()}>
            {map(allTypes, (item, index) => (
              <ListItem
                {...getItemProps({index, item})}
                key={item.id}
                button
                selected={highlightedIndex === index}>
                <Avatar>{item.name[0]}</Avatar>
                    <ListItemText primary={item.name} secondary={`ID: ${item.id}`}/>
              </ListItem>
            ))}
          </Paper>
        )
      }}
    </Query>
  )
}

export default withRouter(SearchType)
