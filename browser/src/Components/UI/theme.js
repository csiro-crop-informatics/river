import React, {Component} from 'react'

import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles'
import * as colors from '@material-ui/core/colors'

const defaultTheme = createMuiTheme({
  palette: {
    primary: colors.deepPurple,
    secondary: colors.cyan,
    error: colors.red,
    type: 'dark',
  },
})

class Colros extends Component {
  render() {
    return (
      <MuiThemeProvider theme={defaultTheme}>
        {this.props.children}
      </MuiThemeProvider>
    )
  }
}

export default Colros
