import React, {Component, Fragment} from 'react'

import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import {map} from 'lodash'
import PropTypes from 'prop-types'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
    marginTop: '-10px',
    marginLeft: '10px',
  },
  margin: {
    marginBottom: theme.spacing.unit * 2,
    backgroundColor: 'green',
  },
  title: {
    display: 'inline-flex',
    width: '100%',
  },
})

class Header extends Component {
  state = {
    anchorEl: null,
  }

  handleClick = event => {
    this.setState({anchorEl: event.currentTarget})
  }

  handleClose = event => {
    this.setState({anchorEl: null})
  }

  render() {
    const {classes} = this.props
    let actions = this.props.actions || []

    let button = null

    if (actions.length === 1) {
      button = (
        <Button onClick={actions[0].action} color="secondary">
          {actions[0].title}
        </Button>
      )
    }

    if (actions.length > 1) {
      button = (
        <Fragment>
          <Button color="secondary" variant="raised" onClick={this.handleClick}>
            {this.props.actionTitle || 'Actions'}
          </Button>
          <Menu
            open={Boolean(this.state.anchorEl)}
            anchorEl={this.state.anchorEl}
            onClose={this.handleClose}>
            {map(this.props.actions, (action, index) => {
              return (
                <MenuItem
                  key={index}
                  onClick={() => {
                    this.handleClose()
                    action.action()
                  }}>
                  {action.title}
                </MenuItem>
              )
            })}
          </Menu>
        </Fragment>
      )
    }

    return (
      <Paper className={classes.root}>
        <div className={classes.title}>
          <Typography gutterBottom variant="headline" style={{flex: 1}}>
            {this.props.title}
          </Typography>
          {button}
        </div>
        {this.props.subtitle && (
          <Typography className={classes.secondaryHeading}>
            {this.props.subtitle}
          </Typography>
        )}

        {this.props.children}
      </Paper>
    )
  }
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  actionTitle: PropTypes.string,
  actions: PropTypes.arrayOf(
    PropTypes.exact({
      title: PropTypes.string.isRequired,
      action: PropTypes.func,
      modal: PropTypes.node,
    }),
  ),
}

export default withStyles(styles)(Header)
