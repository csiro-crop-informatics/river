import React, {Component, Fragment} from 'react'

import Paper from '@material-ui/core/Paper'
import Modal from '@material-ui/core/Modal'
import {withStyles} from '@material-ui/core/styles'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
  modal: {
    padding: theme.spacing.unit * 2,
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    minWidth: '250px',
    width: '600px',
    maxWidth: '80%',
  },
})

class ModalTemplate extends Component {
  handleClose = () => {
    this.setState({open: false})
    if (this.props.handleClose) {
      this.props.handleClose()
    }
  }

  handleOpen = () => {
    this.setState({open: true})
  }

  render() {
    const {classes} = this.props

    return (
      <Fragment>
        <Modal open={this.props.open} onClose={this.handleClose}>
          <Paper className={classes.modal}>{this.props.children}</Paper>
        </Modal>
      </Fragment>
    )
  }
}

export default withStyles(styles)(ModalTemplate)
