import React, {Component, Fragment} from 'react'

import Paper from '@material-ui/core/Paper'
import Modal from '@material-ui/core/Modal'
import {withStyles} from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Error from './Errors'
import {ValidatorForm} from 'react-material-ui-form-validator'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
  modal: {
    padding: theme.spacing.unit * 2,
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    minWidth: '250px',
    width: '600px',
    maxWidth: '80%',
  },
})

class ModalTemplate extends Component {
  state = {
    open: false,
  }

  handleClose = () => {
    this.setState({open: false})
    if (this.props.handleClose) {
      this.props.handleClose()
    }
  }

  handleOpen = () => {
    this.setState({open: true})
  }

  submit = e => {
    e.preventDefault()
    this.props.submit()
  }

  render() {
    const {classes} = this.props

    return (
      <Fragment>
        <span onClick={this.handleOpen}>{this.props.trigger}</span>
        <Modal
          open={this.props.open || this.state.open}
          onClose={this.handleClose}>
          <Paper className={classes.modal}>
            <ValidatorForm onSubmit={this.submit}>
              <Typography variant="headline" gutterBottom>
                {this.props.title}
                {this.props.loading && (
                  <CircularProgress
                    style={{marginLeft: '15px'}}
                    size={25}
                    color="secondary"
                  />
                )}
              </Typography>
              <Divider />
              <div style={{marginTop: '15px', marginBottom: '15px'}}>
                {this.props.error && <Error error={this.props.error} />}
                {this.props.children}
              </div>
              <Divider />
              <div style={{float: 'right', marginTop: '20px'}}>
                <Button
                  onClick={this.handleClose}
                  style={{marginRight: '15px'}}>
                  Cancel
                </Button>

                <Button color="primary" variant="raised" type="submit">
                  Submit
                </Button>
              </div>
            </ValidatorForm>
          </Paper>
        </Modal>
      </Fragment>
    )
  }
}

export default withStyles(styles)(ModalTemplate)
