import React from 'react'

import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'
import {map} from 'lodash'

const styles = theme => ({
  root: {
    marginTop: '30vh',
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const phrases = [
  ['I am a leaf on the wind watch how I soar.'],
  ["No force in the 'verse can stop me."],
  [
    `I came here to drink milk and write software,`,
    `and I've just finished my milk.`,
  ],
  ["I don't have a solution,", 'but I do admire the problem.'],
  ['Well, here I am!', 'What are your other two wishes?'],
  ["Let's cut some red tape...", 'vertically.'],
]

const Home = props => {
  const {classes} = props
  const index = Math.floor(Math.random() * phrases.length)

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <Typography variant="display4" gutterBottom>
          <span role="img" aria-label="River Logo">
            🏞️
          </span>
        </Typography>
      </div>
      <div className={classes.container}>
        <Typography variant="display2" gutterBottom>
          {map(phrases[index], phrase => (
            <div style={{textAlign: 'center'}}>{phrase}</div>
          ))}
        </Typography>
      </div>
    </div>
  )
}
export default withStyles(styles)(Home)
