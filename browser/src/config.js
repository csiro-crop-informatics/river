import {merge} from 'lodash'

const base = {
  graphQLEndpoint:
    process.env.REACT_APP_GRAPHQL_SERVER || 'http://localhost:4000',
}

let env = {}

if (process.env.REACT_APP_ENV === 'develop') {
  env = {
    graphQLEndpoint: process.env.REACT_APP_GRAPHQL_SERVER || '/graphql',
  }
}

if (process.env.REACT_APP_ENV === 'master') {
  env = {
    graphQLEndpoint: process.env.REACT_APP_GRAPHQL_SERVER || '/graphql',
  }
}

export default merge(base, env)
