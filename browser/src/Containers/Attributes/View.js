import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'
import Button from '@material-ui/core/Button'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from 'react-router-dom'

const QUERY = gql`
  query($id: ID!) {
    attribute(where: {id: $id}) {
      id
      name
      hasModel
      isArray
      type
      model {
        id
        name
      }
      parentModels {
        id
        name
      }
    }
  }
`

class View extends Component {
  state = {showRaw: false}
  render() {
    if (this.props.data.loading) {
      return <LinearProgress color="secondary" />
    }

    const {attribute} = this.props.data

    const type = attribute.hasModel ? (
      <Link to={`/models/${attribute.model.id}`} style={{color: 'inherit'}}>
        {attribute.model.name}
      </Link>
    ) : (
      <span>{attribute.type}</span>
    )

    return (
      <React.Fragment>
        <Typography variant="headline">Attribute: {attribute.name}</Typography>
        <Typography component="div" variant="body1">
          Parent:{' '}
          <Link
            style={{color: 'inherit'}}
            to={`/models/${attribute.parentModels[0].id}`}>
            {attribute.parentModels[0].name}
          </Link>
          <br />
          Is List: {attribute.isArray ? 'True' : 'False'} <br />
          Relates to Model: {attribute.hasModel ? 'True' : 'False'} <br />
          Type: {type} <br />
        </Typography>

        <hr />
        <Button
          onClick={() => {
            this.setState({showRaw: !this.state.showRaw})
          }}>
          {' '}
          Show Raw
        </Button>
        {this.state.showRaw && (
          <Typography component="div">
            <pre>{JSON.stringify(this.props.data.attribute, null, 4)}</pre>
          </Typography>
        )}
      </React.Fragment>
    )
  }
}

export default graphql(QUERY, {
  options: props => {
    return {
      variables: {
        id: props.match.params.id,
      },
    }
  },
})(View)
