import React, {Component} from 'react'
import {Switch, Route} from 'react-router-dom'

import View from './View'

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={`${this.props.match.path}/:id`} component={View} />
      </Switch>
    )
  }
}

export default Router
