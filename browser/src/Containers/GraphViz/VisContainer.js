import React, { Component } from "react";

import LinearProgress from "@material-ui/core/LinearProgress";
import DrawComp from "./VisStyles";

import { graphql } from "react-apollo";
import gql from "graphql-tag";

const QUERY = gql`
  query {
    models {
      id
      name
      attributes {
        id
        name
      }
    }
    attributes {
      id
      name
      hasModel
      model {
        id
      }
      parentModels {
        id
      }
    }
  }
`;

class Vis extends Component {
  render() {
    const { data } = this.props;
    const { models, attributes, loading } = data;

    if (loading) {
      return <LinearProgress color="secondary" />;
    }

    return <DrawComp attributes={attributes} models={models} />;
  }
}

export default graphql(QUERY)(Vis);
