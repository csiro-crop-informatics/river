import React, {Component} from 'react'
import {Switch, Route} from 'react-router-dom'

import Vis from './VisContainer'

class Router extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={`${this.props.match.path}`} component={Vis} />
      </Switch>
    )
  }
}

export default Router
