import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'
import Button from '@material-ui/core/Button'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from 'react-router-dom'

const QUERY = gql`
  query($id: ID!) {
    observation(where: {id: $id}) {
      id
      value
      instance {
        id
        name
      }
      attribute {
        id
        name
        hasModel
        model {
          id
          name
          url
        }
      }
      parent {
        id
        name
        model {
          id
          url
          name
        }
      }
      isActive
    }
  }
`

class View extends Component {
  state = {showRaw: false}
  render() {
    if (this.props.data.loading) {
      return <LinearProgress color="secondary" />
    }

    const {observation} = this.props.data

    let value = observation.attribute.hasModel ? (
      <Link
        style={{color: 'inherit'}}
        to={`/instance/${observation.attribute.model.url}/${
          observation.instance.id
        }`}>
        {observation.instance.name}
      </Link>
    ) : (
      observation.value
    )

    return (
      <React.Fragment>
        <Typography variant="headline">Observation</Typography>
        <Typography variant="body1" component="div">
          From:{' '}
          <Link
            style={{color: 'inherit'}}
            to={`/instance/${observation.parent.model.url}/${
              observation.parent.id
            }`}>
            {observation.parent.name}
          </Link>{' '}
          (
          <Link
            style={{color: 'inherit'}}
            to={`/models/${observation.parent.model.id}`}>
            {observation.parent.model.name}
          </Link>
          )<br />
          Value: {value}
          <br />
          Attribute:{' '}
          <Link
            style={{color: 'inherit'}}
            to={`/attributes/${observation.attribute.id}`}>
            {observation.attribute.name}
          </Link>
        </Typography>

        <hr />
        <Button onClick={() => this.setState({showRaw: !this.state.showRaw})}>
          Show Raw
        </Button>
        {this.state.showRaw && (
          <Typography component="div">
            <pre>{JSON.stringify(observation, null, 4)}</pre>
          </Typography>
        )}
      </React.Fragment>
    )
  }
}

export default graphql(QUERY, {
  options: props => {
    return {
      variables: {
        id: props.match.params.id,
      },
    }
  },
})(View)
