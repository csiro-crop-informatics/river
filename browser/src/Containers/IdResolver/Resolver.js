import React, {Component} from 'react'
import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import {ApolloConsumer} from 'react-apollo'
import gql from 'graphql-tag'
import {get} from 'lodash'

const styles = theme => ({
  header: {
    textAlign: 'center',
  },
  form: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  inputDiv: {
    margin: 15,
  },
  input: {
    minWidth: 250,
  },
  button: {
    padding: theme.spacing.unit * 2,
  },
})

const QUERY = gql`
  query($id: ID!) {
    node(id: $id) {
      id
      __typename
      ... on Instance {
        model {
          url
          id
        }
      }
    }
  }
`

class Resolver extends Component {
  state = {
    id: '',
  }

  submit = client => e => {
    e.preventDefault()
    this.setState({showResults: true, loading: true})
    client
      .query({
        query: QUERY,
        variables: {id: this.state.id},
      })
      .then(({data}) => {
        const type = get(data, 'node.__typename')
        const id = get(data, 'node.id')
        const url = get(data, 'node.model.url')

        switch (type) {
          case 'Attribute':
            return this.props.history.push(`/attributes/${id}`)
          case 'Instance':
            return this.props.history.push(`/instance/${url}/${id}`)
          case 'Model':
            return this.props.history.push(`/models/${id}`)
          case 'Observation':
            return this.props.history.push(`/observations/${id}`)
          default:
            return this.props.history.push(`/`)
        }
      })
      .catch(err => console.error(err))
  }

  handleChange = e => {
    this.setState({id: e.target.value})
  }

  render() {
    const {classes} = this.props

    return (
      <ApolloConsumer>
        {client => (
          <React.Fragment>
            <Typography className={classes.header} variant="headline">
              ID Resolver
            </Typography>
            <form className={classes.form} onSubmit={this.submit(client)}>
              <div className={classes.inputDiv}>
                <TextField
                  label="ID"
                  className={classes.input}
                  variant="outlined"
                  onChange={this.handleChange}
                  value={this.state.id}
                />
              </div>
              <div className={classes.buttonDiv}>
                <Button
                  className={classes.button}
                  color="secondary"
                  type="submit"
                  variant="outlined">
                  Search
                </Button>
              </div>
            </form>
          </React.Fragment>
        )}
      </ApolloConsumer>
    )
  }
}

export default withStyles(styles)(Resolver)
