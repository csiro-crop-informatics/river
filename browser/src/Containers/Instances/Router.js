import React, {Component} from 'react'
import {Switch, Route} from 'react-router-dom'

import List from './List'
import View from './View'
import Create from './Create'

class FeatureRouter extends Component {
  render() {
    return (
      <Switch>
        <Route exact path={`${this.props.match.path}/:url`} component={List} />
        <Route
          path={`${this.props.match.path}/:url/create`}
          component={Create}
        />
        <Route
          exact
          path={`${this.props.match.path}/:url/:id`}
          component={View}
        />
      </Switch>
    )
  }
}

FeatureRouter.propTypes = {}

export default FeatureRouter
