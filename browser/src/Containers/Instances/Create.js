import React, {Component} from 'react'

import Error from '../../Components/UI/Errors'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'
import {withStyles} from '@material-ui/core/styles'
import Divider from '@material-ui/core/Divider'

import AttributeInputField from '../../Components/AttributeInputField'

import gql from 'graphql-tag'
import {graphql, compose} from 'react-apollo'
import {map, values, chain} from 'lodash'
import update from 'immutability-helper'

const MUTATION = gql`
  mutation(
    $name: String
    $modelId: ID!
    $observations: [ObservationCreateWithoutParentInput!]
  ) {
    createInstance(
      data: {
        name: $name
        model: {connect: {id: $modelId}}
        observations: {create: $observations}
      }
    ) {
      id
      name
    }
  }
`

const QUERY = gql`
  query($url: String!) {
    model(where: {url: $url}) {
      id
      name
      url
      attributes {
        id
        name
        type
        hasModel
        isArray
        model {
          id
          name
          attributes {
            id
            name
            type
            hasModel
            isArray
          }
        }
      }
    }
  }
`

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
})

class Create extends Component {
  state = {
    form: {},
  }

  onChange = attribute => value => {
    this.setState(state =>
      update(state, {
        form: {[attribute.id]: {$set: value}},
      }),
    )
  }

  submit = e => {
    e.preventDefault()
    const observations = values(this.state.form)

    const nameAttr = chain(this.props)
      .get('data.model.attributes')
      .find({name: 'name'})
      .get('id')
      .value()

    const name = chain(observations)
      .find(o => {
        return o.attribute.connect.id === nameAttr
      })
      .get('value')
      .value()

    this.props
      .mutate({
        variables: {
          observations: observations,
          modelId: this.props.data.model.id,
          name: name,
        },
      })
      .then(data => {
        this.props.history.push(
          `/instance/${this.props.data.model.url}/${
            data.data.createInstance.id
          }`,
        )
      })
  }

  render() {
    const {classes} = this.props
    const {loading, model, error} = this.props.data

    if (loading) {
      return <LinearProgress color="secondary" />
    }

    if (error) {
      return <Error error={error} />
    }

    return (
      <Paper className={classes.root}>
        <Typography variant="headline" gutterBottom>
          Create {model.name}
        </Typography>
        <form onSubmit={this.submit}>
          {map(model.attributes, attribute => {
            return (
              <AttributeInputField
                onChange={this.onChange(attribute)}
                key={attribute.id}
                attribute={attribute}
              />
            )
          })}

          <Divider light style={{marginBottom: 20, marginTop: 20}} />
          <Button type="submit" variant="raised" color="primary">
            Submit
          </Button>
        </form>
      </Paper>
    )
  }
}

export default compose(
  graphql(QUERY, {
    options: props => {
      return {
        variables: {
          url: props.match.params.url,
        },
      }
    },
  }),
  graphql(MUTATION),
)(withStyles(styles)(Create))
