import React, {Component} from 'react'

import LinearProgress from '@material-ui/core/LinearProgress'
import Error from '../../Components/UI/Errors'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'

import gql from 'graphql-tag'
import {graphql, compose} from 'react-apollo'
import {Link} from 'react-router-dom'
import {map, chain} from 'lodash'

const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
})

const QUERY = gql`
  query($url: String!, $id: ID!) {
    instance(where: {id: $id}) {
      id
      name
      observations {
        id
        isActive
        attribute {
          id
          name
        }
        value
        instance {
          id
          name
        }
      }
    }
    model(where: {url: $url}) {
      id
      name
      attributes {
        id
        name
        isArray
        hasModel
        model {
          id
          url
        }
      }
    }
  }
`

class ListInst extends Component {
  state = {showRaw: false}
  render() {
    const {loading, error, instance, model} = this.props.data
    const {classes} = this.props

    if (loading) {
      return <LinearProgress color="secondary" />
    }

    if (error) {
      console.log(error)
      return <Error error={error} />
    }

    return (
      <Paper className={classes.root}>
        <Typography variant="headline">
          {model.name} > {instance.name}
        </Typography>

        <Typography variant="body1" component="div">
          Model:{' '}
          <Link to={`/models/${model.id}`} style={{color: 'inherit'}}>
            {model.name}{' '}
          </Link>
        </Typography>

        <Typography component="div" variant="body1">
          Observations:
          <div style={{paddingLeft: '20px'}}>
            {map(model.attributes, attr => {
              const obs = chain(instance.observations)
                .filter(obs => obs.attribute.id === attr.id && obs.isActive)
                .value()

              const dispModel = obs => {
                if (!obs) {
                  return <span>n/a</span>
                }
                return (
                  <span key={obs.id}>
                    <Link
                      to={`/observations/${obs.id}`}
                      style={{color: 'inherit'}}>
                      Observation
                    </Link>{' '}
                    →{' '}
                    <Link
                      to={`/instance/${attr.model.url}/${obs.instance.id}`}
                      style={{color: 'inherit'}}>
                      {obs.instance.name}
                    </Link>
                  </span>
                )
              }

              const dispVal = obs => {
                if (!obs) {
                  return <span>n/a</span>
                }
                return (
                  <span key={obs.id}>
                    <Link
                      to={`/observations/${obs.id}`}
                      style={{color: 'inherit'}}>
                      Observation
                    </Link>{' '}
                    → {obs.value}
                  </span>
                )
              }

              const fn = !attr.hasModel ? dispVal : dispModel

              if (!attr.isArray) {
                return (
                  <div key={attr.id}>
                    <Link
                      to={`/attributes/${attr.id}`}
                      style={{color: 'inherit'}}>
                      {attr.name}
                    </Link>
                    : {fn(obs[0])}
                  </div>
                )
              }

              return (
                <div key={attr.id}>
                  <Link
                    to={`/attributes/${attr.id}`}
                    style={{color: 'inherit'}}>
                    {attr.name}
                  </Link>
                  : <br />
                  <div style={{paddingLeft: 20}}>{map(obs, o => fn(o))}</div>
                  {obs.length === 0 && (
                    <div style={{paddingLeft: 20}}>No {attr.name}</div>
                  )}
                </div>
              )
            })}
          </div>
        </Typography>

        <hr />
        <Button
          onClick={() => {
            this.setState({showRaw: !this.state.showRaw})
          }}>
          Show Raw
        </Button>
        {this.state.showRaw && (
          <Typography component="div">
            <pre>{JSON.stringify(instance, null, 4)}</pre>
          </Typography>
        )}
      </Paper>
    )
  }
}

export default compose(
  graphql(QUERY, {
    options: props => {
      return {
        variables: {
          url: props.match.params.url,
          id: props.match.params.id,
        },
      }
    },
  }),
)(withStyles(styles)(ListInst))
