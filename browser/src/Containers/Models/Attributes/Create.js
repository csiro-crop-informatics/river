import React, {Component} from 'react'

import {map} from 'lodash'

import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'

import NameForm from './FormComponents/Name'
import TypeForm from './FormComponents/TypeSearch'
import UnitForm from './FormComponents/UnitSearch'
import DisplayForm from './FormComponents/DisplayForm'

import update from 'immutability-helper'

const DEFAULT_PHENOMENON = {
  name: '',
  hasModel: true,
  isArray: false,
  model: null,
}

const steps = ['Name Phenomemon', 'Connect', 'Display Options']

class CreatePhenomenon extends Component {
  state = {
    activeStep: 0,
    phenomenon: DEFAULT_PHENOMENON,
    useConnect: true,
    loading: false,
    error: false,
  }

  handleBack = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }))
  }

  handleNext = () => {
    if (!this.state.phenomenon.hasModel && this.state.activeStep === 0) {
      this.setState(state => ({
        activeStep: state.activeStep + 2,
      }))
    } else {
      this.setState(state => ({
        activeStep: state.activeStep + 1,
      }))
    }
  }

  render() {
    const {activeStep, phenomenon} = this.state
    const measure = phenomenon.defaultMeasureId || ''

    let step

    switch (activeStep) {
      case 0:
        step = (
          <NameForm
            submit={this.handleNext}
            phenomenon={phenomenon}
            updateName={e => {
              const name = e.target.value
              this.setState(state =>
                update(state, {phenomenon: {name: {$set: name}}}),
              )
            }}
            updateHasType={() => {
              this.setState(state =>
                update(state, {phenomenon: {$toggle: ['hasModel']}}),
              )
            }}
          />
        )
        break
      case 1:
        if (phenomenon.hasModel) {
          step = (
            <TypeForm
              phenomemon={phenomenon}
              submit={this.handleNext}
              back={this.handleBack}
              updateType={value => {
                this.setState(state =>
                  update(state, {phenomenon: {type: {$set: value}}}),
                )
              }}
            />
          )
        } else {
          step = (
            <UnitForm
              phenomenon={phenomenon}
              submit={this.handleNext}
              back={this.handleBack}
              measure={measure}
              updateUnit={value => {
                this.setState(state =>
                  update(state, {phenomenon: {unit: {$set: value}}}),
                )
              }}
              updateMeasure={e => {
                this.setState(state =>
                  update(state, {
                    phenomenon: {
                      defaultMeasureId: {$set: e.target.value},
                    },
                  }),
                )
              }}
            />
          )
        }
        break
      case 2:
        step = (
          <DisplayForm
            phenomenon={phenomenon}
            updateIsArray={() => {
              this.setState(state =>
                update(state, {phenomenon: {$toggle: ['isArray']}}),
              )
            }}
            submit={() => this.props.submit(phenomenon)}
            back={this.handleBack}
          />
        )
        break
      default:
        break
    }

    return (
      <React.Fragment>
        <Stepper activeStep={activeStep}>
          {map(steps, (step, index) => {
            return (
              <Step key={index}>
                <StepLabel>{step}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
        <div style={{paddingTop: '25px', paddingBottom: '25px'}}>{step}</div>
      </React.Fragment>
    )
  }
}

export default CreatePhenomenon
