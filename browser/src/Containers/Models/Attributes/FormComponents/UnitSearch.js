import React from 'react'

import {ValidatorForm} from 'react-material-ui-form-validator'

import {withStyles} from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
import Divider from '@material-ui/core/Divider'

const styles = theme => ({
  contentPadding: {
    padding: theme.spacing.unit * 2,
  },
  gutterBottom: {
    paddingBottom: theme.spacing.unit * 4,
  },
  divider: {
    marginTop: '25px',
    marginBottom: '25px',
  },
})

const UnitSearch = props => {
  const {classes} = props
  return (
    <ValidatorForm onSubmit={props.submit}>
      <Divider className={classes.divider} />
      <div style={{float: 'right'}}>
        <Button onClick={props.back}>Back</Button>
        <Button type="submit" variant="contained" color="primary">
          Next
        </Button>
      </div>
    </ValidatorForm>
  )
}

export default withStyles(styles)(UnitSearch)
