import React from 'react'

import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator'
import {withStyles} from '@material-ui/core/styles'
import FormLabel from '@material-ui/core/FormLabel'
import Tooltip from '@material-ui/core/Tooltip'
import Help from '@material-ui/icons/Help'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import Divider from '@material-ui/core/Divider'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  contentPadding: {
    padding: theme.spacing.unit * 2,
  },
  gutterBottom: {
    paddingBottom: theme.spacing.unit * 4,
  },
  divider: {
    marginTop: '25px',
    marginBottom: '25px',
  },
})

const Name = props => {
  const {classes, phenomenon} = props
  return (
    <ValidatorForm onSubmit={props.submit}>
      <div className={classes.gutterBottom}>
        <TextValidator
          label="Name"
          name="Name"
          fullWidth
          variant="outlined"
          helperText="A label for how the phenomenon will be displayed"
          value={phenomenon.name}
          validators={['required']}
          errorMessages={['This field is required']}
          onChange={props.updateName}
        />
      </div>
      <FormLabel component="legend">
        Phenomenon Type
        <Tooltip
          placement="right"
          title="If the phenomenon references another type turn the is type field on. If the phenomenon references a scalar or an array of scalars leave the switch off.">
          <Help />
        </Tooltip>
      </FormLabel>
      <FormControlLabel
        control={
          <Switch
            checked={phenomenon.hasModel}
            onChange={props.updateHasType}
          />
        }
        label="Is Type"
      />
      <Divider className={classes.divider} />
      <div style={{float: 'right'}}>
        <Button type="submit" variant="contained" color="primary">
          Next
        </Button>
      </div>
    </ValidatorForm>
  )
}

export default withStyles(styles)(Name)
