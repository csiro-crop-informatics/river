import gql from 'graphql-tag'

export const listFragment = gql`
  fragment listAttributes on Attribute {
    id
    name
    hasModel
    model {
      id
      name
    }
    parentModels {
      id
      name
    }
  }
`
