import React, {Component} from 'react'

import LinearProgress from '@material-ui/core/LinearProgress'
import CreateModal from './CreateModal'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Avatar from '@material-ui/core/Avatar'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Header from '../../Components/UI/Header'

import {Link} from 'react-router-dom'

import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {map} from 'lodash'

const TYPES = gql`
  query models {
    models {
      id
      name
      attributes {
        id
        name
      }
    }
  }
`

class ListOntologies extends Component {
  state = {
    createModalOpen: false,
  }

  openModal = () => {
    this.setState({createModalOpen: true})
  }

  closeModal = () => {
    this.setState({createModalOpen: false})
  }

  render() {
    const {loading, error, models} = this.props.data

    if (loading) {
      return <LinearProgress color="secondary" />
    }

    if (error) {
      return <pre>{JSON.stringify(error, null, 4)}</pre>
    }

    return (
      <React.Fragment>
        <Header
          title="Models"
          actions={[
            {
              title: 'Create',
              action: this.openModal,
            },
          ]}>
          <List>
            {map(models, model => (
              <ListItem
                key={model.id}
                to={`models/${model.id}`}
                component={Link}
                button>
                <Avatar>{model.name[0]}</Avatar>
                <ListItemText
                  primary={model.name}
                  secondary={`${model.attributes.length} attributes`}
                />
              </ListItem>
            ))}
          </List>
          {!models.length && <Typography>No models created</Typography>}
        </Header>
        <CreateModal
          open={this.state.createModalOpen}
          closeModal={this.closeModal}
        />
      </React.Fragment>
    )
  }
}

export default graphql(TYPES, {
  options: {
    pollInterval: 1000,
  },
})(ListOntologies)
