import React, {Component} from 'react'

import {Query} from 'react-apollo'
import gql from 'graphql-tag'
import Downshift from 'downshift'
import {withRouter} from 'react-router-dom'
import Error from '../../../Components/UI/Errors'

import Paper from '@material-ui/core/Paper'
import ListItem from '@material-ui/core/ListItem'
import Avatar from '@material-ui/core/Avatar'
import ListItemText from '@material-ui/core/ListItemText'
import CircularProgress from '@material-ui/core/CircularProgress'
import {TextValidator} from 'react-material-ui-form-validator'
import map from 'lodash/map'

const SEARCH = gql`
  query models($input: String!) {
    models(where: {name_contains: $input}, first: 10) {
      id
      name
    }
  }
`

class SearchType extends Component {
  render() {
    return (
      <Downshift
        itemToString={item => (item ? item.name : '')}
        onChange={this.props.onChange}>
        {({
          inputValue,
          getInputProps,
          getMenuProps,
          getItemProps,
          selectedItem,
          highlightedIndex,
          isOpen,
        }) => {
          return (
            <div>
              <TextValidator
                label="Type"
                name="Type"
                variant="outlined"
                fullWidth
                validators={['required']}
                errorMessages={['Field is required']}
                margin="normal"
                {...getInputProps()}
              />
              <AutoCompleteMenu
                {...{
                  inputValue,
                  getMenuProps,
                  getItemProps,
                  selectedItem,
                  highlightedIndex,
                  isOpen,
                  history: this.props.history,
                }}
              />
            </div>
          )
        }}
      </Downshift>
    )
  }
}

function AutoCompleteMenu({
  selectedItem,
  highlightedIndex,
  isOpen,
  getItemProps,
  getMenuProps,
  inputValue,
  history,
}) {
  if (!isOpen) {
    return null
  }

  return (
    <Query query={SEARCH} variables={{input: inputValue}}>
      {({loading, error, data}) => {
        const allTypes = (data && data.models) || []

        if (loading) {
          return (
            <Paper>
              <ListItem>
                <Avatar>
                  <CircularProgress size={25} color="secondary" />
                </Avatar>
                <ListItemText primary="Loading" />
              </ListItem>
            </Paper>
          )
        }

        if (error) {
          return <Error error={error} />
        }

        return (
          <Paper {...getMenuProps()}>
            {map(allTypes, (item, index) => (
              <ListItem
                {...getItemProps({index, item})}
                key={item.id}
                button
                selected={highlightedIndex === index}>
                <Avatar>{item.name[0]}</Avatar>
                <ListItemText primary={item.name} />
              </ListItem>
            ))}
          </Paper>
        )
      }}
    </Query>
  )
}

export default withRouter(SearchType)
