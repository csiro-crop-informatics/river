import React, {Component} from 'react'

import gql from 'graphql-tag'
import {graphql, compose} from 'react-apollo'
import {filter} from 'lodash'

import CreateattributeModal from './Attributes/CreateModal'
import LinearProgress from '@material-ui/core/LinearProgress'
import Header from '../../Components/UI/Header'
import Error from '../../Components/UI/Errors'
import List from '@material-ui/core/List'
import Avatar from '@material-ui/core/Avatar'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import IconButton from '@material-ui/core/IconButton'
import Delete from '@material-ui/icons/Delete'

import {get, map} from 'lodash'
import {listFragment} from './Attributes/fragments'

const QUERY = gql`
  query model($id: ID!) {
    model(where: {id: $id}) {
      id
      name
      attributes {
        ...listAttributes
      }
    }
  }
  ${listFragment}
`

const MUTATION = gql`
  mutation($modelId: ID!, $attributeId: ID!) {
    updateModel(
      where: {id: $modelId}
      data: {attributes: {disconnect: {id: $attributeId}}}
    ) {
      id
      name
      attributes {
        ...listAttributes
      }
    }
  }
  ${listFragment}
`

class UpdateTypes extends Component {
  state = {
    openModal: false,
  }

  closeModal = () => {
    this.setState({openModal: false})
  }

  openModal = isChild => () => {
    this.setState({openModal: true, filter: filter})
  }

  render() {
    const {loading, error, model} = this.props.data

    if (loading) {
      return <LinearProgress color="secondary" />
    }

    if (error) {
      return <Error error={this.props.data.error} />
    }

    return (
      <React.Fragment>
        <Header title={model.name} />
        <Header
          title="Attributes"
          actions={[{title: 'Create', action: this.openModal(false)}]}>
          <List>
            {map(model.attributes, attribute => {
              let type = get(attribute, 'model.name')
              let char = get(type, '[0]', 'x')

              return (
                <ListItem button key={attribute.id}>
                  <Avatar>{char}</Avatar>
                  <ListItemText primary={attribute.name} secondary={type} />
                  <ListItemSecondaryAction
                    onClick={() => {
                      this.props.mutate({
                        variables: {
                          attributeId: attribute.id,
                          modelId: model.id,
                        },
                      })
                    }}>
                    <IconButton aria-label="Delete">
                      <Delete />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              )
            })}
          </List>
        </Header>
        <CreateattributeModal
          type={model}
          open={this.state.openModal}
          closeModal={this.closeModal}
        />
      </React.Fragment>
    )
  }
}

export default compose(
  graphql(QUERY, {
    options: props => {
      return {
        pollInterval: 1000,
        variables: {
          id: props.match.params.id,
        },
      }
    },
  }),
  graphql(MUTATION),
)(UpdateTypes)
