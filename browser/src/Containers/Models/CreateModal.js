import React, {Component} from 'react'
import Modal from '../../Components/UI/Modal'
import {TextValidator} from 'react-material-ui-form-validator'

import gql from 'graphql-tag'
import {graphql, compose} from 'react-apollo'
import {withRouter} from 'react-router-dom'
import {kebabCase} from 'lodash'

const QUERY = gql`
  query models {
    models {
      id
      name
      url
      attributes {
        id
      }
    }
  }
`

const CREATE_TYPE = gql`
  mutation createModel($data: ModelCreateInput!) {
    createModel(data: $data) {
      id
      name
      url
      attributes {
        id
      }
    }
  }
`

class Create extends Component {
  state = {
    name: '',
    loading: false,
    error: false,
  }

  updateName = e => {
    this.setState({name: e.target.value})
  }

  submit = mutation => {
    this.setState({loading: true, error: null})
    this.props
      .mutate({
        variables: {
          data: {name: this.state.name, url: kebabCase(this.state.name)},
        },
      })
      .then(data => {
        this.setState({loading: false, name: ''})
        this.props.closeModal()
      })
      .catch(error => {
        this.setState({
          loading: false,
          error: error,
        })
      })
  }

  render() {
    return (
      <Modal
        title="Create a Type"
        handleClose={this.props.closeModal}
        open={this.props.open}
        loading={this.state.loading}
        error={this.state.error}
        submit={this.submit}>
        <TextValidator
          fullWidth
          label="Name"
          name="Name"
          variant="outlined"
          validators={['required']}
          errorMessages={['This field is required']}
          value={this.state.name}
          onChange={this.updateName}
        />
      </Modal>
    )
  }
}

export default compose(
  graphql(CREATE_TYPE, {
    options: {
      update: (proxy, {data: {createModel}}) => {
        const data = proxy.readQuery({query: QUERY})

        data.models.push(createModel)

        proxy.writeQuery({query: QUERY, data: data})
      },
    },
  }),
)(withRouter(Create))
