import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import client from './apollo-client'

import {ApolloProvider} from 'react-apollo'

import Theme from './Components/UI/theme'

ReactDOM.render(
  <ApolloProvider client={client}>
    <Theme>
      <App />
    </Theme>
  </ApolloProvider>,
  document.getElementById('root'),
)
registerServiceWorker()
