import {ApolloClient} from 'apollo-client'

import {InMemoryCache} from 'apollo-cache-inmemory'

import {HttpLink} from 'apollo-link-http'
import {ApolloLink} from 'apollo-link'
import QueueLink from 'apollo-link-queue'
import {RetryLink} from 'apollo-link-retry'
import config from './config'
import fragmentMatcher from './fragmentMatcher'
import {onError} from 'apollo-link-error'

const offlineLink = new QueueLink()

const cache = new InMemoryCache({fragmentMatcher})

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({graphQLErrors, networkError}) => {
      if (graphQLErrors)
        graphQLErrors.map(({message, locations, path}) =>
          console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
          ),
        )
      if (networkError) console.log(`[Network error]: ${networkError}`)
    }),
    new RetryLink(),
    offlineLink,
    new HttpLink({
      uri: config.graphQLEndpoint || '/graphql',
    }),
  ]),
  cache: cache,
})

export default client
