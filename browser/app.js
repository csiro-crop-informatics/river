/// This file is only used in production. This allows for you to specify
/// any River Graphql server at rutime using the GRAPHQL_SERVER env variable

const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const proxy = require('express-http-proxy')

const app = express()
const port = process.env.PORT || 5000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use((req, res, next) => {
  next()
})

// API calls
app.use(
  '/graphql',
  proxy(
    process.env.GRAPHQL_SERVER || 'https://graphql.tomorrowstodayslater.xyz',
  ),
)

// Serve any static files
app.use(express.static(path.join(__dirname, 'build')))

// Handle React routing, return all requests to React app
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

app.listen(port, () => console.log(`Listening on port ${port}`))
